## LinkedOut - A "Job Search Simulator" Game
![LinkedOut Title Screen](../main/screens/splash.png)

This project is a work-in-progress made during my brief bursts of so-called "Free Time".
In a nutshell, this is a game of sorts where the player desperately has to search for a job before they can no longer afford rent.

Essentially, you have an ever-growing index of job listings that you can scroll through at your leisure.
Respond to them, attend interviews, and try your best to get a job offer.

### Desktop Shell
LinkedOut comes with a fakeOS built right into the game. This includes form fields, dropdowns, and window management.
When you go to log in for the very first time, you'll go through a resume system to register your user account.  
![Filling out a form](../main/screens/linkedout-shell.gif)  
  
The main application you'll be using in LinkedOut is the LinkedOut Job Explorer, which uses a bespoke dispatching system to randomly deliver
new jobs to the player over the course of the in-game day.  
  
![Job Explorer](../main/screens/job-search.gif)
  
  
### Current Goals

* Development of a RPG-like "Resume" creator, complete with stats and a fake job history.
* Incorporation of a "Test Interview", which will evaluate stats and change potential interactions
* Development of some kind of time-based limit where the player runs out of money completely
* Degrading Potential - the longer you look for a job, the worse the available offers get
* Some kind of win condition - accept at least one offer.
