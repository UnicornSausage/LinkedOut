// main global script file
Listing jobs[100];
Organization companies[100];
Expense bills[1000];
int jobCount = 0; //This is incremented on creation of job listings and assigns index values in jobs array
int billCount = 0;
int companyCount = 0; // Companies need a tally system, too.
bool firstTime = true; //Players can back out to main menu, so we need this to avoid triggering second dispatch of jobs
int jobRoom = 0;
int profileCount = 0;
int dispatchCount = 0;
String clock;
String date;
String minute;
String dateTime;

function autoSave() {
    String profile = String.Format("%s %s", playerfn, playerln);
    SaveGameSlot(profileCount, profile);
}


function checkTime() {
  if (DayNight.IsTimeRunning() == true) {
      date = String.Format("Day %d", DayNight.GetDay());
      clock = String.Format("%02d:%02d",
                                    DayNight.GetHour(),
                                    DayNight.GetMinute());
      dateTime = String.Format("%s %s", date, clock);
      timeClock.Text = dateTime;
  }
}

function checkGUI() {
  if (GUI.GetAtScreenXY(mouse.x, mouse.y)) {
    hoverOver = GUI.GetAtScreenXY(mouse.x, mouse.y);
      if ((hoverOver == gExplorer)) {
        hoverDesc.Text = "gExplorer";
        DragDrop.AutoTrackHookKey = true;
        if (Mouse.IsButtonDown(eMouseLeft)) {
          gExplorer.ZOrder = 10;
          gDetails.ZOrder = 1;
          gResume.ZOrder = 1;
          gConfirmation.ZOrder = 1;
          gDropdown.ZOrder = 1;
          
        }
      }
      else if ((hoverOver == gDetails)) {
        hoverDesc.Text = "gDetails";
        DragDrop.AutoTrackHookKey = true;
        if (Mouse.IsButtonDown(eMouseLeft)) {
          gExplorer.ZOrder = 1;
          gDetails.ZOrder = 10;
          gResume.ZOrder = 1;
          gConfirmation.ZOrder = 1;
          gDropdown.ZOrder = 1;
          
        }
      }
      else if ((hoverOver == gResume)) {
        hoverDesc.Text = "gResume";
        DragDrop.AutoTrackHookKey = true;
        if (Mouse.IsButtonDown(eMouseLeft)) {
          gExplorer.ZOrder = 1;
          gDetails.ZOrder = 1;
          gResume.ZOrder = 9;
          gConfirmation.ZOrder = 1;
          gDropdown.ZOrder = 10;
          
        }
      }
      else if ((hoverOver == gConfirmation)) {
       hoverDesc.Text = "gConfirmation";
       DragDrop.AutoTrackHookKey = true;
        if (Mouse.IsButtonDown(eMouseLeft)) {
          gExplorer.ZOrder = 1;
          gDetails.ZOrder = 1;
          gResume.ZOrder = 1;
          gConfirmation.ZOrder = 10;
          gDropdown.ZOrder = 1;
          
        }
      }
      else if ((hoverOver == gDropdown)) {
       hoverDesc.Text = "gDropdown";
       DragDrop.AutoTrackHookKey = false;
        if (Mouse.IsButtonDown(eMouseLeft)) {
          gExplorer.ZOrder = 1;
          gDetails.ZOrder = 1;
          gResume.ZOrder = 1;
          gConfirmation.ZOrder = 1;
          gDropdown.ZOrder = 10;
          
        }
      }
      else if ((hoverOver == gMainMenu)) {
       hoverDesc.Text = "gMainMenu";
       DragDrop.AutoTrackHookKey = false;
      }
  }
  else {
    hoverOver = null;
    hoverDesc.Text = "";
    DragDrop.AutoTrackHookKey = false;
  }
}


function setDropdown(GUIControl *parent) {
    gDropdown.X = gResume.X + parent.X;
    gDropdown.Y = gResume.Y + parent.Y;
    gDropdown.Width = parent.Width;
    dropList.Width = parent.Width;
}


enum popupOptions {
  Info, 
  Warning
};

function popUp(popupOptions param, String popupTitle, String popupBody) {
  if (param == Info) {
    statusIcon.NormalGraphic = 30;
  }
  else if (param == Warning) {
    statusIcon.NormalGraphic = 31;
  }
  confirmationTitle.Text = popupTitle;
  confirmationBody.Text = popupBody;
  gConfirmation.ZOrder = 10;
  gConfirmation.TweenPosition(0.3, 68, 54, eEaseInSineTween, eBlockTween);
}

function closePopup() {
  gConfirmation.TweenPosition(0.3, -200, 0, eEaseOutSineTween, eBlockTween);
}

function invokeBill(float cost, String reason) {
  int i = billCount;
  bills[i].expCost = cost;
  bills[i].expRef = reason;
  String explanation = String.Format("Price: $%.2f, Reason: %s", cost, reason);
  billCount++;
  balance = balance - cost;
  popUp(Info, "New Bill", explanation);
}

function updateJobs() {
  // Update the Counter
  jCount.Text = String.Format("Jobs: %d", dispatchCount);
  // Next, refresh the list
  if (jobList.ItemCount > 0) {
    jobList.Clear();
  }
  // Now, list all created jobs the game knows about
  for (int i = 0; i < jobCount; i++) {
    if (jobs[i].Dispatched == true) {
      jobList.AddItem(jobs[i].Meta);
    }
  }
  
}

enum tierOptions {
  Top, 
  Mid, 
  Low
};

function createJob(tierOptions param, String companyName, String jobPosition, String jobDesc, int Payment, String Rate,  int interviewRoom) {
  // This is a neat little wrapper function for a Struct object in AGS.
  // Set the index value in an array to whatever the jobCount value is - starts at zero.
  int i = jobCount;
  
  // Pull values in from the createJob function params
  jobs[i].orgEntity = companyName;
  jobs[i].Position = jobPosition;
  jobs[i].Description = jobDesc;
  jobs[i].Salary = Payment;
  jobs[i].Interval = Rate;
  jobs[i].Room = interviewRoom;
  
  // Assign a Tier value
  
  if (param == Top) {
    jobs[i].Tier = 3;
  }
  else if (param == Mid) {
    jobs[i].Tier = 2;
  }
  else if (param == Low) {
    jobs[i].Tier = 1;
  }
  
  // Generate a collated data value for job listings in the Explorer GUI
  jobs[i].Meta = String.Format("%s | %s | $%d/%s ", companyName, jobPosition, Payment, Rate);
  jobs[i].Dispatched = false;
  
  // Increment job count by one
  jobCount++;
  
  // Run the updateJobs function to clear the "inbox" and "refresh" it.
  updateJobs();
}

function createCompany(int logo, String name, String sector, String saying, String bio){
  int i = companyCount;
  companies[i].icon = logo;
  companies[i].orgName = name;
  companies[i].Industry = sector;
  companies[i].Slogan = saying;
  companies[i].History = bio;
  
  // Increment Company Count by 1
  companyCount++;
}

function beginSearch() {
  gResume.Visible = true;
  popUp(Info, "Welcome!", "Welcome to LinkedOut. Before you can begin searching for jobs, you'll need to fill out your digital resume.");
  // We have to do this eval so that starting the game, closing Explorer, and opening Explorer again doesn't
  // cause the game to endlessly create the same jobs over and over again.
  if (firstTime == true) {
    firstTime = false;
  }
}

function checkNewjobs() {
  // This is largely a test to see if jobs can be assigned to the Explorer based on certain times of the day.
    if (jobCount > dispatchCount) { // Only update if the amount of jobs available is higher than the amount sent out.
      for (int i = 0; i < newRange; i++) { //This needs to somehow only grab like 3 jobs at random
          if (jobs[i].Dispatched == false) {
            jobs[i].Dispatched = true;
            dispatchCount++; // This seems to increment even if there are no new jobs available...
          }
      }
      if (rangeSet == false) { // This is a logic-gate so that the game doesn't try to increment range a thousand times in 1 second
       newRange = newRange + 3; // Slide the range up over and over.
       rangeSet = true; // Close the gate
      }
       updateJobs();
    }
}

function seedJobs() {
  createJob(Top, "Axiom Inc", "Data Analyst", "Axiom Inc, industry leader of the Artificial Intelligence space, is seeking a data analyst to help oversee the management of our business intelligence pipeline. We're looking for a motivated self-starter looking to tighten our operational loop. ", 80000, "yr", 2);
  createJob(Top, "EverX", "Smart Contract Writer", "This isn't a Real Job.", 33, "yr",2);
  createJob(Top, "Complex", "Penetration Tester", "This isn't a Real Job.", 130000, "yr",2);
  createJob(Top, "Quangle", "Software Engineer", "This isn't a Real Job.", 130000, "yr",2);
  createJob(Top, "Spiral", "Game Programmer", "This isn't a Real Job.", 130000, "yr",2);
  createJob(Top, "WellSpring", "Backend Engineer", "This isn't a Real Job.", 130000, "yr",2);
}

function seedCompanies() {
  createCompany(42, "Axiom Inc", "Consumer AI", "Deeply examining the smallest bits!", "Axiom Inc is a startup that applies Machine Learning to Big Data.");
  createCompany(43, "Quangle", "Social Network", "Whose side are you on?", "Quangle is a social platform designed around debating strangers online.");
  createCompany(47, "EverX", "Cryptocurrencies", "It's a long climb to the top.", "EverX is a crypto exchange that offers DeFi contracts for revenue sharing.");
  createCompany(44, "Complex", "OpsSec", "Think inside the box.", "Complex is a technology startup focused on providing security solutions to other businesses.");
  createCompany(45, "Spiral", "Game Studio", "Beyond Twisted.", "Spiral is the studio best known for Hyperspace II: Arc Beyond Time.");
  createCompany(46, "WellSpring", "FinTech", "WellSpring is a platform designed to make 401K Benefits accessible to everyone. ", "Violence company");
}

function setPronoun() {
  if (playergd == "Male") {
    playerpn = "Mr.";
  }
  else if (playergd == "Female") {
    playerpn = "Ms.";
  }
  else if (playergd == "Non-Binary") {
    playerpn = "Mx.";
  }
}

function setEducation() {
  // We already have a String value for education, but an integer is just easier to check on the backend.
  // We can also use this to set a "ceiling" or "floor" on value check to see
  // If players are over/under qualified for a position.
  if (playeredu == "Some High School") {
    edu_max = 1;
  }
  else if (playeredu == "High School Graduate") {
    edu_max = 2;
  }
  else if (playeredu == "Some College") {
    edu_max = 3;
  }
  else if (playeredu == "Associate's Degree") {
    edu_max = 4;
  }
  else if (playeredu == "Bachelor's Degree") {
    edu_max = 5;
  }
  else if (playeredu == "Master's Degree") {
    edu_max = 6;
  }
  else if (playeredu == "PhD") {
    edu_max = 7;
  }
}

function enableLauncher() {
  gLauncher.Transparency = 100;
  gLauncher.TweenPosition(3.0, 35, 73, eEaseInSineTween, eNoBlockTween);
  gLauncher.TweenTransparency(3.0, 0, eEaseInSineTween, eNoBlockTween);
}

function disableLauncher() {
  gLauncher.TweenTransparency(6.0, 100, eEaseInSineTween, eNoBlockTween);
  gLauncher.TweenPosition(3.0, 400, 73, eEaseInSineTween, eBlockTween);
  gLauncher.Transparency = 100;
}

function enableInfo() {
  gInfo.Transparency = 100;
  gInfo.TweenPosition(2.0, 0, 0, eEaseInSineTween, eNoBlockTween);
  gInfo.TweenTransparency(2.0, 0, eEaseInSineTween, eNoBlockTween);
}

function disableInfo() {
  gInfo.TweenTransparency(2.0, 100, eEaseInSineTween, eNoBlockTween);
  gInfo.TweenPosition(2.0, 0, -20, eEaseInSineTween, eNoBlockTween);
}

function clearResume() {
  // This basically sets all of the Player variables back to nothing.
  // It serves as a "Log Out" utility function that drops all data
  // and resets to default values.
  playerfn = "";
  playerln = "";
  playereth = "";
  playergd = "";
  playeredu = "";
  playerspec = "";
  balance = 10000.00;
  fieldEthnic.Text = "Ethnicity";
  fieldGender.Text = "Gender";
  fieldSpecialty.Text = "Specialty";
  fieldEducation.Text = "Education";
  fnEdit.Text = "First Name";
  lnEdit.Text = "Last Name";
  firstName.Text = "";
  lastName.Text = "";
}

function lookupCompany(String companyName) {
    for (int i = 0; i < companyCount; i++) {
      if (companies[i].orgName == companyName) {
        selected_logo = companies[i].icon;
        selected_slogan = String.Format(companies[i].Slogan);
        selected_sector = companies[i].Industry;
        selected_history = companies[i].History;
        selected_industry = companies[i].Industry;
        
        companyLogo.NormalGraphic = selected_logo;
        companyBio.Text = selected_history;
        industry.Text = selected_sector;
      }
   }
   
}


// called when the game starts, before the first room is loaded
function game_start()
{
  Mouse.Mode = eModePointer;
  DragDrop.Enabled = true;
  DragDropCommon.ModeEnabled[eDragDropGUI] = true;
  DragDropCommon.DragMove = eDDCmnMoveSelf;
  DragDrop.AutoTrackHookKey = false;
  DragDrop.DefaultHookKey = 0;
  DragDrop.DefaultHookMouseButton = eMouseLeft;
  DragDrop.DragMinDistance = 10;
  DragDrop.DragMinTime = 0;
  setMode(Menu);
}

// called on every game cycle, except when the game is blocked
function repeatedly_execute()
{
  balanceInd.Text = String.Format("Balance: $%.2f", balance);
  
}

// called on every game cycle, even when the game is blocked
function repeatedly_execute_always()
{
  checkGUI();
  checkTime();
  if (DayNight.IsTimeRunning() == true) {
      if (DayNight.GetMinute() == 5) { // This needs to be rewritten to account for a reasonable recurring interval
        checkNewjobs();
      }
      
        if (DayNight.GetMinute() == 10) {
        rangeSet = false;  // After waiting a few seconds, set open the gate again.
      }
      
  }
  
  if (ddParent != null) {
    setDropdown(ddParent);
  }
  
  
}

// called when a key is pressed
function on_key_press(eKeyCode keycode)
{
  if (IsGamePaused())
  {
    // game paused, so don't react to any keypresses
    keycode = 0;
  }
  else if (keycode == eKeyCtrlQ)
  {
    // Ctrl-Q will quit the game
    QuitGame(1);
  }
  else if (keycode == eKeyF9)
  {
    // F9 will restart the game
    RestartGame();
  }
  else if (keycode == eKeyF12)
  {
    // F12 will save a screenshot to the save game folder
    SaveScreenShot("screenshot.pcx");
  }
  else if (keycode == eKeyCtrlS)
  {
    // Ctrl-S will give the player all defined inventory items
    Debug(0, 0);
  }
  else if (keycode == eKeyCtrlV)
  {
    // Ctrl-V will show game engine version and build date
    Debug(1, 0);
  }
  else if (keycode == eKeyCtrlA)
  {
    // Ctrl-A will show walkable areas
    Debug(2, 0);
  }
  else if (keycode == eKeyCtrlX)
  {
    // Ctrl-X will let the player teleport to any room
    Debug(3, 0);
  }
  else if ((keycode == eKeyUpArrow) && (gDropdown.Visible == true)) {
    //If dropdown is visible, players can scroll up
    dropList.ScrollUp();
    //Move the selection cursor up by one value for each keypress
    dropList.SelectedIndex =  dropList.SelectedIndex -1;
  }
  else if ((keycode == eKeyDownArrow) && (gDropdown.Visible == true)) {
    //If dropdown is visible, players can scroll down
    dropList.ScrollDown();
    //Move the selection cursor down by one value for each keypress
    dropList.SelectedIndex =  dropList.SelectedIndex +1;
  }
}

// called when a mouse button is clicked
function on_mouse_click(MouseButton button)
{
  if (IsGamePaused())
  {
    // game is paused, so do nothing (i.e. don't process mouse clicks)
  }
  else if (button == eMouseLeft)
  {  
     // left-click, so try using the current mouse cursor mode at this position
      Room.ProcessClick(mouse.x, mouse.y, mouse.Mode);
  }
  else if (button == eMouseRight)
  {
    // For now, no declarations at all.
  }
  else if ((button == eMouseWheelNorth) && (gDropdown.Visible == true)) {
    dropList.ScrollUp();
    dropList.SelectedIndex =  dropList.SelectedIndex -1;
  }
  else if ((button == eMouseWheelSouth) && (gDropdown.Visible == true)) {
    dropList.ScrollDown();
    dropList.SelectedIndex =  dropList.SelectedIndex +1;
  }
}

function dialog_request(int param) {
}

function jobList_OnSelectionChanged(GUIControl *control)
{
 // Declare some String and int values based on click selection
 String job_desc = jobs[jobList.SelectedIndex].Description;
 String job_meta = jobs[jobList.SelectedIndex].Meta;
 int job_salary = jobs[jobList.SelectedIndex].Salary;
 String job_rate = jobs[jobList.SelectedIndex].Interval;
 String job_position = jobs[jobList.SelectedIndex].Position;
 String job_company = jobs[jobList.SelectedIndex].orgEntity;
 jobRoom = jobs[jobList.SelectedIndex].Room;
 
 // Change some labels to match the values we just created.
 detailDesc.Text = job_desc;
 jobHeading.Text = String.Format("%s - %s", job_position, job_company);
 rateHeading.Text = String.Format("Income: $%d/%s", job_salary, job_rate);
 
 // Open the Details pop-up.
 selected_company = job_company;
 lookupCompany(selected_company);
 gDetails.ZOrder = 10;
 gExplorer.ZOrder = 9;
 gDetails.Visible = true;
}

function closeDetails_OnClick(GUIControl *control, MouseButton button)
{
 gDetails.Visible = false;
}

function Button2_OnClick(GUIControl *control, MouseButton button)
{
  gDetails.Visible = false;
}

function closeExplorer_OnClick(GUIControl *control, MouseButton button)
{
  gExplorer.Visible = false;
}

function Quit_OnClick(GUIControl *control, MouseButton button)
{
  QuitGame(0);
}

function Search_OnClick(GUIControl *control, MouseButton button)
{
  beginSearch();
}

function Respond_OnClick(GUIControl *control, MouseButton button)
{
  // We fetch a global variable that was set before the window was opened.
  setMode(Interview);
  player.ChangeRoom(jobRoom);
}

// This section uses some logic to imitate form inputs. 
// It's rather tedious - basically, you hide a button on top of a top
// of a text parser, steal the input, then replace the button label
// The player's first and last name get updated in the process.

function fnEdit_OnClick(GUIControl *control, MouseButton button)
{
  fnEdit.Visible = false;
  firstName.Enabled = true;
  validateFn.Visible = true; // Little Enter Key arrow, prompting player to hit Enter.
  if (lastName.Enabled == true) {
    Game.SimulateKeyPress(eKeyReturn);
    lastName.Enabled = false;
    lnEdit.Text = playerln;
    lnEdit.Visible = true;
  }
}

function firstName_OnActivate(GUIControl *control)
{
 if (IsKeyPressed(eKeyReturn)) { 
   String resfn = firstName.Text;
   playerfn = resfn;
   firstName.Text = resfn;
   firstName.Enabled = false;
   fnEdit.Text = resfn;
   fnEdit.Visible = true;
   validateFn.Visible = false;
   
    if (firstName.Text == "") {
     fnEdit.Text = "First Name";
   }
   
 }
}

function lnEdit_OnClick(GUIControl *control, MouseButton button)
{
 lnEdit.Visible = false;
 lastName.Enabled = true;
 validateLn.Visible = true; // Little Enter Key arrow, prompting player to hit Enter.
 if (firstName.Enabled == true) {
   Game.SimulateKeyPress(eKeyReturn);
   firstName.Enabled = false;
   fnEdit.Text = playerfn;
   fnEdit.Visible = true;
 }
}

function lastName_OnActivate(GUIControl *control)
{
  if (IsKeyPressed(eKeyReturn)) { 
   String resln = lastName.Text;
   playerln = resln;
   lastName.Text = resln;
   lastName.Enabled = false;
   lnEdit.Text = resln;
   lnEdit.Visible = true;
   validateLn.Visible = false;
   
   if (lastName.Text == "") {
     lnEdit.Text = "Last Name";
   }
 }
}

function resumeConfirm_OnClick(GUIControl *control, MouseButton button)
{
  // Kinda sloppy attempt at Forms Validation - basic rule is as follows:
  // If any of the Resume fields are empty, simply prompt the user when the player tries to click OK
  if ((playergd == "") || (playeredu == "") || (playereth == "") || (playerfn == "") || (playerln == "") || (playerspec == "")) {
      // This part is kind of hacky, and only calls out one field at a time instead of everything that's missing.
      if ((playerfn == "")) {
        popUp(Warning, "User Error!", "Missing field: First Name");
      }
      else if ((playerln == "")) {
        popUp(Warning, "User Error!", "Missing field: Last Name");
      }
      else if ((playergd == "")) {
        popUp(Warning, "User Error!", "Missing field: Gender");
      }
      else if ((playereth == "")) {
        popUp(Warning, "User Error!", "Missing field: Ethnicity");
      }
      else if ((playerspec == "")) {
        popUp(Warning, "User Error!", "Missing field: Specialty");
      }
      else if ((playeredu == "")) {
        popUp(Warning, "User Error!", "Missing field: Education");
      }
  }
  else {
    // Neat little hack - instead of manually checking strings and doing branch logic
    // We instead set up other global variables to set pronouns and education
    setPronoun(); //playerpn
    setEducation(); //edu_max - This value is used for interviewers to check against with basic algebra.
    gResume.Visible = false;
    infoName.Text = String.Format("%s %s", playerfn, playerln);
    gInfo.Visible = true;
    gMainMenu.Visible = false;
    enableInfo();
    enableLauncher();
    Wait(40);
    profileCount++;
    invokeBill(50.0, "Subscription Fee to LinkedOut");
    DayNight.SetTime(1, 8, 0);
    DayNight.SetTimeSpeed(40);
    DayNight.SetTimeRunning(true);
    seedJobs();
    seedCompanies();
    autoSave(); // It might be a good idea to create an initial save upon profile creation
  }
}

function fillDropdown(GUIControl *parent) {
  if (parent == fieldGender) {
    dropList.AddItem("Male");
    dropList.AddItem("Female");
    dropList.AddItem("Non-Binary");
    gDropdown.Height = 30;
  }
  else if (parent == fieldEthnic) {
    dropList.AddItem("Asian");
    dropList.AddItem("Arabic");
    dropList.AddItem("Black / African American");
    dropList.AddItem("Caucasian");
    dropList.AddItem("Hispanic / Latino");
    dropList.AddItem("Pacific Islander");
    gDropdown.Height = 50;
  }
  else if (parent == fieldEducation) {
    dropList.AddItem("Some High School");
    dropList.AddItem("High School Graduate");
    dropList.AddItem("Some College");
    dropList.AddItem("Associate's Degree");
    dropList.AddItem("Bachelor's Degree");
    dropList.AddItem("Master's Degree");
    dropList.AddItem("PhD");
    gDropdown.Height = 90;
  }
  else if (parent == fieldSpecialty) {
    dropList.AddItem("Administration");
    dropList.AddItem("Accountant");
    dropList.AddItem("Architect");
    dropList.AddItem("Community Manager");
    dropList.AddItem("Customer Operations");
    dropList.AddItem("Data Analyst");
    dropList.AddItem("Engineering");
    dropList.AddItem("Human Resources");
    dropList.AddItem("Information Technology");
    dropList.AddItem("Marketing");
    dropList.AddItem("Sales");
    dropList.AddItem("Writing");
    gDropdown.Height = 90;
  }
}

function fieldGender_OnClick(GUIControl *control, MouseButton button)
{
  if (gDropdown.Visible == false) {
    ddParent = fieldGender;
    fillDropdown(ddParent);
    setDropdown(ddParent);
    gDropdown.Visible = true;
  }
  else {
    gDropdown.Visible = false;
    dropList.Clear();
  }
}

function dropList_OnSelectionChanged(GUIControl *control)
{
  if (ddParent == fieldGender) {
    playergd = dropList.Items[dropList.SelectedIndex];
    gDropdown.Visible = false;
    fieldGender.Text = String.Format("%s", playergd);
    dropList.Clear();
  }
  else if (ddParent == fieldEthnic) {
    playereth = dropList.Items[dropList.SelectedIndex];
    gDropdown.Visible = false;
    fieldEthnic.Text = String.Format("%s", playereth);
    dropList.Clear();
  }
    else if (ddParent == fieldSpecialty) {
    playerspec = dropList.Items[dropList.SelectedIndex];
    gDropdown.Visible = false;
    fieldSpecialty.Text = String.Format("%s", playerspec);
    dropList.Clear();
  }
    else if (ddParent == fieldEducation) {
    playeredu = dropList.Items[dropList.SelectedIndex];
    gDropdown.Visible = false;
    fieldEducation.Text = String.Format("%s", playeredu);
    dropList.Clear();
  }
}

function fieldEthnic_OnClick(GUIControl *control, MouseButton button)
{
    if (gDropdown.Visible == false) {
    ddParent = fieldEthnic;
    setDropdown(ddParent);
    fillDropdown(ddParent);
    gDropdown.Visible = true;
  }
  else {
    gDropdown.Visible = false;
    dropList.Clear();
  }
}

function fieldSpecialty_OnClick(GUIControl *control, MouseButton button)
{
  if (gDropdown.Visible == false) {
    ddParent = fieldSpecialty;
    fillDropdown(ddParent);
    setDropdown(ddParent);
    gDropdown.Visible = true;
  }
  else {
    gDropdown.Visible = false;
    dropList.Clear();
  }
}

function fieldEducation_OnClick(GUIControl *control, MouseButton button)
{
  if (gDropdown.Visible == false) {
    ddParent = fieldEducation;
    fillDropdown(ddParent);
    setDropdown(ddParent);
    gDropdown.Visible = true;
  }
  else {
    gDropdown.Visible = false;
    dropList.Clear();
  }
}

function launchExplorer_OnClick(GUIControl *control, MouseButton button)
{
  gExplorer.Visible = true;
}

function launchExit_OnClick(GUIControl *control, MouseButton button)
{
  disableInfo();
  disableLauncher();
  clearResume();
  DayNight.SetTimeRunning(false);
  gMainMenu.Visible = true;
}

function launchHelp_OnClick(GUIControl *control, MouseButton button)
{
  // We'll eventually have a GUI built for this
  popUp(Info, "We're Sorry!", "The Help System is unavailable in the current build.");
}

function launchFinance_OnClick(GUIControl *control, MouseButton button)
{
  // We'll also have a GUI built for this soon
  popUp(Info, "We're Sorry!", "The Finance Tracker is unavailable in the current build.");
}
function confirmationOK_OnClick(GUIControl *control, MouseButton button)
{
  closePopup();
}

function confirmationCancel_OnClick(GUIControl *control, MouseButton button)
{
  closePopup();
}

function resumeSearch_OnClick(GUIControl *control, MouseButton button)
{
  RestoreGameDialog();
}
