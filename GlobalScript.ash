// Main header script - this will be included into every script in
// the game (local and global). Do not place functions here; rather,
// place import definitions and #define names here to be used by all
// scripts.

struct Listing {
  String orgEntity;
  String Position;
  String Description;
  int Salary;
  String Interval;
  int Room;
  String Meta;
  bool Dispatched;
  int Tier;
};

struct Organization {
  int icon;
  String orgName;
  String Industry;
  String Slogan;
  String History;
};

struct Expense {
  float expCost;
  String expRef;
};

enum modeOption {
  fakeOS, 
  Interview, 
  Menu
};

function setMode(modeOption param) {
  
  if (param == fakeOS) {
    currentMode = "fakeOS";
    gExplorer.Visible = true;
    gWordMark.Visible = true;
    gLauncher.Visible = true;
    gInfo.Visible = true;
    gConfirmation.Visible = true;
    gCopyleft.Visible = true;
    gMainMenu.Visible = false;
    
  }
  else if (param == Interview) {
    currentMode = "Interview";
    gExplorer.Visible = false;
    gDetails.Visible = false;
    gWordMark.Visible = false;
    gLauncher.Visible = false;
    gInfo.Visible = false;
    gConfirmation.Visible = false;
    gCopyleft.Visible = false;
    gMainMenu.Visible = false;
  }
  else if (param == Menu) {
    currentMode = "Menu";
    gExplorer.Visible = false;
    gWordMark.Visible = false;
    gLauncher.Visible = false;
    gDetails.Visible = false;
    gInfo.Visible = false;
    gConfirmation.Visible = true;
    gCopyleft.Visible = false;
    gMainMenu.Visible = true;
  }
}

function exitInterview() {
  setMode(fakeOS);
  player.ChangeRoom(1);
}